/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useMemo, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Dropdown from "react-bootstrap/Dropdown";
import { useSelector } from "react-redux";
import objectPath from "object-path";
import { useHtmlClassService } from "../../../_core/MetronicLayout";
import { toAbsoluteUrl } from "../../../../_helpers";
import { DropdownTopbarItemToggler } from "../../../../_partials/dropdowns";
// import SVG from "react-inlinesvg";
import axios from 'axios';

export function QUser() {
  const { user } = useSelector(state => state.auth);
  const [pegawai, setPegawai] = useState([]);

  const getDataPegawai = async () => {
    return await axios.get(`https://127.0.0.1:8000${user.pegawai['@id']}`)
  }

  useEffect(() => {
    getDataPegawai()
    .then(({data: {jabatanPegawais}})=> {
      setPegawai(jabatanPegawais)
    })
    .catch((e)=>{
      console.log(e)
    })

  }, []);


  const uiService = useHtmlClassService();
  const layoutProps = useMemo(() => {
    return {
      light:
        objectPath.get(uiService.config, "extras.user.dropdown.style") ===
        "light"
    };
  }, [uiService]);

  const showJabatanPegawai = () => {
    return pegawai.map((data, index)=> (
      <div key={index}>
      <div key={index}  className="navi-item px-8">
            <div key={data.jabatan['@id']} className="navi-text">
              <h5 key={data.atribut}  className="font-weight-bold">Jabatan</h5>
              <div key={data.jabatan['@type']}  className="text-muted">
                {data.jabatan.nama}
              </div>
            </div>
          </div>
          <div key={data.unit['@id']}  className="navi-separator mt-3"></div>
          <br />
          <div key={data.unit['@type']}  className="navi-item px-8">
            <div key={data.kantor['@id']}  className="navi-text">
              <h5 key={data.tipe}  className="font-weight-bold">Kantor</h5>
              <div key={data.kantor['@type']}  className="text-muted">
                {data.kantor.nama}
              </div>
            </div>
          </div>
          <br />
          <div key={data.referensi}  className="navi-separator mt-3"></div>

          </div>
    ))
  }



  return (
    <Dropdown drop="down" alignRight>
      <Dropdown.Toggle
        as={DropdownTopbarItemToggler}
        id="dropdown-toggle-user-profile"
      >
        <div
          className={
            "btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2"
          }
        >
          <span className="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">
            Selamat Datang,
          </span>{" "}
          <span className="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">
            {/*{user.detail.username}*/}
            {user.pegawai.nama}
          </span>
          <span className="symbol symbol-35 symbol-light-success">
            <span
              className="symbol-label font-size-h5 font-weight-bold"
              style={{ backgroundColor: "#FFC91B", borderRadius:'50px' }}
            >
              {/*<span className="svg-icon svg-icon-xl svg-icon-warning">*/}
              {/*  <SVG src={toAbsoluteUrl("/media/svg/icons/General/User.svg")} />*/}
              {/*</span>*/}
              <i className="flaticon2-user text-white"></i>
            </span>
          </span>
        </div>
      </Dropdown.Toggle>
      <Dropdown.Menu className="p-0 m-0 dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
        <>
          {/** ClassName should be 'dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl' */}
          {layoutProps.light && (
            <>
              <div className="d-flex align-items-center p-8 rounded-top">
                <div className="symbol symbol-md bg-light-primary mr-3 flex-shrink-0">
                  <img src={toAbsoluteUrl("/media/users/300_21.jpg")} alt="" />
                </div>
                <div className="text-dark m-0 flex-grow-1 mr-3 font-size-h5">
                  {/*{user.detail.username}*/}
                  {user.username}
                </div>
                <span className="label label-light-success label-lg font-weight-bold label-inline">
                  3 messages
                </span>
              </div>
              <div className="separator separator-solid"></div>
            </>
          )}

          {!layoutProps.light && (
            <div
              className="d-flex align-items-center justify-content-between flex-wrap p-8 bgi-size-cover bgi-no-repeat rounded-top"
              style={{
                backgroundImage: `url(${toAbsoluteUrl("/media/misc/bg-1.jpg")})`
              }}
            >
              <div className="symbol bg-white-o-15 mr-3">
                {/*<span className="symbol-label text-success font-weight-bold font-size-h4">*/}
                {/*  S*/}
                {/*</span>*/}
                {/*<img alt="Pic" className="hidden" src={user.pic} />*/}
                {/*<span className="symbol-label text-success font-weight-bold font-size-h4">*/}
                <span
                    className="symbol-label font-size-h5 font-weight-bold"
                    style={{ backgroundColor: "white" }}
                >
                  {/*<span className="svg-icon svg-icon-xl svg-icon-warning">*/}
                  {/*  <SVG*/}
                  {/*    src={toAbsoluteUrl("/media/svg/icons/General/User.svg")}*/}
                  {/*  />*/}
                  {/*</span>*/}

                  <i className="flaticon2-user text-warning"></i>
                </span>
              </div>
              <div className="text-white m-0 flex-grow-1 mr-3 font-size-h5">
               {/*{user.detail.username}*/}
                {user.pegawai.nama}
              </div>
            </div>
          )}
        </>

        <div className="navi navi-spacer-x-0 pt-5">
          {showJabatanPegawai()}
          {/* <div className="navi-item px-8">
            <div className="navi-text">
              <h5 className="font-weight-bold">Jabatan</h5>
              <div className="text-muted">
                Pelaksana Seksi Pengembangan Sistem Pendukung Manajemen
              </div>
            </div>
          </div>
          <div className="navi-separator mt-3"></div>
          <br />
          <div className="navi-item px-8">
            <div className="navi-text">
              <h5 className="font-weight-bold">Kantor</h5>
              <div className="text-muted">
                Direktorat Teknologi Informasi dan Komunikasi
              </div>
            </div>
          </div> */}

          {/* <div className="navi-separator mt-3"></div> */}

          <div className="navi-footer  px-8 py-5">
            <Link
              to="/logout"
              className="btn btn-light-primary font-weight-bold"
            >
              Logout
            </Link>
          </div>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  );
}

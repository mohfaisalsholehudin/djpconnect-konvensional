/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import "../../../../_metronic/_assets/sass/pages/login/classic/login-1.scss";
import { LayoutSplashScreen } from "../../../../_metronic/layout";
import { useSelector } from "react-redux";

import qs from "qs";
import axios from "axios";
import { connect } from "react-redux";
import * as auth from "../_redux/authRedux";

function AuthPage(props) {
  const { iamToken } = useSelector(state => state.auth);
  const { REACT_APP_SSO_URL, REACT_APP_IAM_URL, REACT_APP_LOCAL_URL} = process.env;

  const str = "djpconnect:djpconnect";
  const pass = btoa(str);
  const [token, setToken] = useState("");

  const data = {
    grant_type: "authorization_code",
    code: props.code,
    redirect_uri: `${REACT_APP_LOCAL_URL}/auth/login`
  };

  const config = {
    headers: {
      "Content-type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${pass}`
    }
  };

  const dataClient = {
    clientId: "djpconnect",
    clientSecret: "djpconnect",
    token
  };

  const LOGIN_URL = `${REACT_APP_IAM_URL}/api/authentication`;
  // const LOGIN_URL = `/api/authentication`;

  //After hit this function, you will get access_token
  const postCodeToSso = async () => {
    return await axios.post(
      `${REACT_APP_SSO_URL}/oauth/token`,
      qs.stringify(data),
      config
    );
  };

  //Check access_token to SSO, and you will get user data
  const checkToken = async () => {
    return await axios.post(
      `${REACT_APP_SSO_URL}/oauth/check_token`,
      qs.stringify(dataClient),
      config
    );
  };

  const getLogin = async () => {
    const username = "admin";
    const password = "admin";
    return axios.post(LOGIN_URL, { username, password });
  };

  const [loading] = useState(true);

  // const disableLoading = () => {
  //   setLoading(false);
  // };

  useEffect(() => {
    const response = postCodeToSso();
    response
      .then(({ data: { access_token } }) => {
        setToken(access_token);
        //set token sso
        props.sso(access_token);
        getLogin()
          .then(({ data: { token } }) => {
            //set token IAM
            props.getTokenIam(token);
          })
          .catch(e => {
            console.log(e);
          });
      })
      .catch(e => {
        console.log(e);
      });
  }, []);

  useEffect(() => {
    // let mount = true;
    if (iamToken !== undefined) {
      // getUser();
      const response = checkToken();
    response
      .then(({ data, status }) => {
        if (status === 200) {
          const configIam = {
            headers: {
              "Content-type": "application/json",
              Authorization: `Bearer ${iamToken}`
            }
          };
          axios
            .get(
              `${REACT_APP_IAM_URL}/api/users/${data.user_name}`,
              // `/api/users/${data.user_name}`,
              configIam
            )
            .then(({ data }) => {
                props.fulfillUser(data);
             
            })
            .catch(e => {
              console.log(e);
            });
        }
      })
      .catch(e => {
        console.log(e);
      });
    } 
    
    // return() => {
    //   mount = false;
    // }
  }, [iamToken]);

  return (
    <>
      {loading ? (
        <LayoutSplashScreen />
      ) : (
        <div className="d-flex flex-column flex-root">
          <div
            className="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white"
            id="kt_login"
          >
            <div
              className="flex-row-fluid d-flex flex-column position-relative p-7 overflow-hidden"
              style={{ backgroundColor: "#212C5F" }}
            >
              <div
                className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0"
                style={{ backgroundColor: "#212C5F" }}
              ></div>
              <div className="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5"></div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default withRouter(connect(null, auth.actions)(AuthPage));

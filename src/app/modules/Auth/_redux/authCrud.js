import axios from "axios";


//Unused when using SSO
export const LOGIN_URL = "IAM_URL";

export function login(username, password) {
  return axios.post(LOGIN_URL, { username, password});
}

export const getUserByToken = () => {
  // Authorization head should be fulfilled in interceptor.
  return axios.post(`IAM_URL`);
}


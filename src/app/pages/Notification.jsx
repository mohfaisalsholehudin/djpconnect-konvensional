import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next'
import {
  Card,
  CardBody
} from '../../../src/_metronic/_partials/controls';
import {Card as card} from 'react-bootstrap';

import Slide from '@material-ui/core/Slide';
function Notification() {
   
const products = [{
    id: '1',
    name: 'John',
    price: '10000'
}]
  const columns = [
    {
      dataField: 'id',
      text: 'Product ID'
    },
    {
      dataField: 'name',
      text: 'Product Name'
    },
    {
      dataField: 'price',
      text: 'Product Price'
    }
  ]
  return (
    <>
    <Slide direction="up" in={true} mountOnEnter unmountOnExit>
    <Card style={{
              borderLeft: "10px solid #1D428A",
              borderRight: "10px solid #1D428A",
              borderBottom: "10px solid #1D428A",
              borderTop: "1px solid #1D428A"
            }}>
        <card.Header style={{
                backgroundColor: "#1D428A",
                fontWeight: "700",
                borderRadius: "0px"
              }}>
            <div className="card-title">
                <div className="card-label" style={{color:'#ffffff'}}>
                    Notifikasi
                </div>

            </div>
        </card.Header>
        <CardBody>
          <BootstrapTable
            keyField='id'
            data={products}
            columns={columns}
            bordered={false}
            search
          />
        </CardBody>
      </Card>
    </Slide>
      
    </>
  )
}

export default Notification;